// [Section] Document Object Model (DOM)
/*
	- It allows us to access or modify the properties of an html element in a webpage
	- We use this because it is standard on how to get, change, add, or delete html elements.
	- We will focus on using DOM in managing forms.

	- Syntax:
		document.querySelector("html element")
	- For selecting HTML elements, we will be using document.querySelector
	- The quesrySelector function takes a string input that is formatted like a css selector when applying the styles
*/
	const txtFirstName = document.querySelector("#txt-first-name");
	// console.log(txtFirstName);

	const txtLastName = document.querySelector("#txt-last-name");

	const name = document.querySelectorAll(".full-name");
	// console.log(name);

	const span = document.querySelectorAll("span");
	// console.log(span);

	const text = document.querySelectorAll("input[type]");
	// console.log(text);

	const spanFullName = document.querySelector("#fullName");

	/*Examples (1):

	const div = document.querySelectorAll(".first-div > span");
	console.log(div);
	const second = document.querySelector(".first-div > .second-span");
	console.log(second);*/



// [Section] Event Listeners
/*
	- Whenever a user interacts with a webpage, this action is considered as an event.
	- Working with events is large part of creating interactivity in a webpage.
	- Specific functions that will perform an action

	- The function use is "addEventListener" - it has two arguments
		1. First argument -  a string identifying an event. Will declare an event.
		2. Second argument - function that the listener will trigger once the "specific event" is triggered. 

		*keyup - trigger when you release the key
		*keydown - trigger when you press the key even without releasing
		*change - type then enter to trigger
*/
	txtFirstName.addEventListener("keyup", (event) => {
		// console.log(event.target.value)
	})


	const fullName = () => {
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	}
		txtFirstName.addEventListener("keyup", fullName);
		txtLastName.addEventListener("keyup", fullName);


/*---------- Activity S47 (12-13-2022) ----------*/
	const colorList = document.querySelector("#colors")

	/*colorList.addEventListener("click", (event) => {
		spanFullName.style.colorList = event.target.value
	})*/

	const changeColor = () => {
		spanFullName.style.color = `${colorList.value}`
	}
		colorList.addEventListener("click", changeColor);
